#!/bin/bash
#cxfreeze-2.6 pycessing.py --target-dir Pycessing.app --icon=./icons/app-icon_48x48x32.png
rm -rf build dist
python setup.py py2app --resources pycessing --iconfile pycessing/data/icons/app-icon.icns
cp -r /opt/local/lib/Resources/qt_menu.nib dist/pycessing.app/Contents/Resources/
#cp *.py dist/pycessing.app/Contents/Resources/
#cp freesansbold.ttf dist/pycessing.app/Contents/Resources/
