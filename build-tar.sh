#!/bin/bash
rm -rf /tmp/pycessing
mkdir /tmp/pycessing
cp *.py /tmp/pycessing
cp build-* /tmp/pycessing
cp COPYING /tmp/pycessing
cp -r icons /tmp/pycessing
cp -r examples /tmp/pycessing
cp -r help /tmp/pycessing
cd /tmp
tar -cvf pycessing.tar pycessing
gzip pycessing.tar
