PyCessing
==========

PyCessing is a simple Python-based framework that makes it easy
for those that are new to programming to play with graphics and sound.
It is inspired by Processing.org.

Homepage: http://www.pycessing.org
License: GPLv3

Dependencies
---------------
Python 2
setuptools
PyGame
PyQt4
python-qtscintilla
python-cairo


Running from source
--------------------
To run PyCessing directly from the source tree, do

  ./scripts/pycessing


Building & Installing
------------------------
PyCessing uses setuptools/distutils to create Python packages.

  python setup.py install
