"""
    pycess.py
    Copyright 2009 Brendan Howell (brendan@howell-ersatz.com)

    This file is part of PyCessing.

    PyCessing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCessing is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCessing.  If not, see <http://www.gnu.org/licenses/>.
"""


# this is the main wrapper class for pycessing
# it stores all of the global settings as well as provides 
# utilities for basic window, sound and display settings.

# TODO: icon on mac & pc

import pygame
from pkg_resources import resource_filename

import os
import platform
import sys

class Store:
    def __init__(self):
        return

class Pycess:
    def __init__(self, filename=""):
        self.framerate = 60
        self.noLoop = False
        self.screen = None
        self.fullscreen = False
        
        self.display_w = 320
        self.display_h = 240
        self.clock = pygame.time.Clock()
        self._setup_done = False
        self.keyPressed = False
        self._numKeysPressed = 0
        self.key = None
        self.filename = filename
        
    def size(self, x=320, y=240, fullscreen=False, flags=pygame.OPENGL):
        print "setting display size"
        self.display_w = x
        self.display_h = y
        
        if fullscreen:
            flags = pygame.FULLSCREEN
        else:
            flags = 0  # disable openGL for now
        
        self.screen = pygame.display.set_mode((x,y), flags, 0)
        pygame.display.set_caption('PyCessing - ' + self.filename, "PyCessing")
        module_location = os.path.dirname(__file__)
        

        iconfile = resource_filename('pycessing', "data/icons/app-icon.ico")
        try:
            icon = pygame.image.load(iconfile)
        except pygame.error, e:
            print "Warning: Unable to load application icon: %s" % e
        else:
            pygame.display.set_icon(icon)

        #else:
            #icon = pygame.image.load(os.path.join(module_location,"icons", "app-icon.icns"))
        #pygame.display.set_icon(icon)
        self._setup_done = True

        
    def doEvents(self, eventlist):
        for event in eventlist:
            try:
                if event.type == pygame.QUIT:
                    sys.exit(0)
                elif event.type == pygame.KEYDOWN:
                    self._numKeysPressed += 1
                    self.keyPressed = True
                    self.key = event.unicode
                    self.onKeyDown(event)
                elif event.type == pygame.KEYUP:
                    self._numKeysPressed -= 1
                    if self._numKeysPressed < 1:
                        self.keyPressed = False
                    self.onKeyUp(event)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.onMousePressed()
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.onMouseReleased()
                elif event.type == pygame.JOYBUTTONDOWN:
                    self.onJoyButtonPressed()
                elif event.type == pygame.JOYBUTTONUP:
                    self.onJoyButtonReleased()
                
            except AttributeError:
                pass
        
    def isMousePressed(self):
        button1, button2, button3 = pygame.mouse.get_pressed()
        return button1 or button2 or button3
        
    def getMouseX(self):
        return pygame.mouse.get_pos()[0]
        
    def getMouseY(self):
        return pygame.mouse.get_pos()[1]
        
    def getWidth(self):
        return self.display_w

    def getHeight(self):
        return self.display_h 

    def getFPS(self):
        return self.clock.get_fps()
