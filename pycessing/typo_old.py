"""
    typo.py
    Copyright 2012 Brendan Howell (brendan@howell-ersatz.com)

    This file is part of PyCessing.

    PyCessing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCessing is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCessing.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygame
import os
from itertools import chain

SCREEN = None
PROJECT_DIR = None
 
def truncline(text, font, maxwidth):
    """wraps a line to fit the font and spits out the rest"""
    real=len(text)       
    stext=text           
    l=font.size(text)[0]
    cut=0
    a=0                  
    done=1
    old = None
    while l > maxwidth:
        a=a+1
        n=text.rsplit(None, a)[0]
        if stext == n:
            cut += 1
            stext= n[:-cut]
        else:
            stext = n
        l=font.size(stext)[0]
        real=len(stext)               
        done=0                        
    return real, done, stext             
        
def wrapline(text, font, maxwidth): 
    done=0                      
    wrapped=[]                  
                               
    while not done:             
        nl, done, stext = truncline(text, font, maxwidth) 
        wrapped.append(stext.strip())                  
        text=text[nl:]                                 
    return wrapped 
 
def wrap_multi_line(text, font, maxwidth):
    """ returns text taking new lines into account."""
    lines = chain(*(wrapline(line, font, maxwidth) for line in text.splitlines()))
    return list(lines)

class ScreenText:
    def __init__(self, text=None, fontName=None, size=30, color=(255,255,255)):
        self.text=text
        self.fontName = fontName
        self.surface = None
        self.setFont(fontName, size)
        self.color = color

    def setText(self, text):
        self.text = text

    def setFont(self, fontName, size=30):
        if (fontName):
            fnt = pygame.font.match_font(fontName)
        else:
            print "Warning: font not found.  Using default."
            fnt = "freesansbold.ttf"
        self.fontName = fontName
        self.font = pygame.font.Font(fnt, size)
        self.size = size
        
    def setFontFromFile(self, filename, size=30):
        #fp = open(filename)
        if not(os.path.exists(filename)):
            filename = os.path.join(PROJECT_DIR,filename)
            if not(os.path.exists(filename)):
                print "Error: font file " + filename + " not found."
                return
        self.font = pygame.font.Font(filename, size)
        self.size = size

    def setSize(self, size):
        self.size = size
        self.setFont(self.fontName, size)

    def setColor(self, *args):
        if len(args) == 1:
            self.color = args[0]
        elif len(args) == 3:
            self.color = (args[0], args[1], args[2])
        elif len(args) == 4:
            self.color = (args[0], args[1], args[2], args[3])
        else:
            print "ScreenText: error invalid color specified."

    def draw(self, x, y, sx=None, sy=None, width=None, height=None):
        self.surface = self.font.render(self.text, True, self.color)
        SCREEN.unlock()
        SCREEN.blit(self.surface, (x, y))
        
    def draw_wrapped(self, x, y, max_width, lineheight=None):
        if not(lineheight):
            lineheight = self.size 
        for line in wrapline(self.text, self.font, max_width):
            self.surface = self.font.render(line, True, self.color)
            SCREEN.unlock()
            SCREEN.blit(self.surface, (x, y))
            y += lineheight
        
    def getFonts(self):
        print pygame.font.get_fonts()
        
    def getWidth(self):
        w,h = self.font.size(self.text)
        return w
        
    def getHeight(self):
        w,h = self.font.size(self.text)
        return h

    def setItalic(self,val=True):
        self.font.set_italic(val)

    def setUnderline(self, val=True):
        self.font.set_underline(val)

    def setBold(self, val=True):
        self.font.set_bold(val)
