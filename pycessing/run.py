"""
    run.py
    Copyright 2012 Brendan Howell (brendan@howell-ersatz.com)

    This file is part of PyCessing.

    PyCessing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCessing is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCessing.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygame
import sys
import os

from pycess import *

from drawing_cairo import *
import images
import typo
from images import Image
from sounds import Sound
from typo import ScreenText


def setup():
    pass

def draw():
    pass


#TODO: move these functions into drawing modules

def circle(x, y, radius):
    drawing.circle(x, y, radius)

def rect(x, y, width, height):
    drawing.rect(x, y, width, height)

def ellipse(cx, cy, width, height):
    drawing.ellipse(cx, cy, width, height)

def arc(cx, cy, radius, startAngle, endAngle):
    drawing.arc(cx, cy, radius, startAngle, endAngle)

def line(x1, y1, x2, y2):
    drawing.line(x1, y1, x2, y2)

def polygon(pointlist):
    drawing.polygon(pointlist)

def curve(x1, y1, cx1, cy1, cx2, cy2, x2, y2):
    drawing.curve(x1, y1, cx1, cy1, cx2, cy2, x2, y2)

def setBackground(red, green=None, blue=None):
    drawing.setBackground(red, green, blue)

def setStroke(red, green=None, blue=None, alpha=255, width=1):
    drawing.setStroke(red, green, blue, alpha, width)

def setStrokeColor(red, green=None, blue=None, alpha=255):
    drawing.setStrokeColor(red, green, blue, alpha)

def setStrokeWidth(width):
    drawing.setStrokeWidth(width)

def setFillColor(red, green=None, blue=None, alpha=255):
    drawing.setFillColor(red, green, blue, alpha)

def setFillState(fillState):
    drawing.setFillState(fillState)

def setFillOn():
    drawing.setFillOn()

def setFillOff():
    drawing.setFillOff()

def screenGrab(fileName):
    drawing.screenGrab(fileName)

def getPixel(x, y):
    return pycessing.screen.get_at((x, y))

def setPixel(x, y, r, g, b, a=None):
    if (a):
        pycessing.screen.set_at((x, y), (r, g, b, a))
    else:
        pycessing.screen.set_at((x, y), (r, g, b))



def main(cess_file, project_dir=None):
    """Initialize engine, execute .cess file and start """

    global pycessing
    global drawing

    infile = open(cess_file)
    filename = os.path.basename(cess_file)

    pygame.init()
    pygame.fastevent.init()

    pycessing = Pycess(filename)
    drawing = Drawing()

    if project_dir:
        os.chdir(project_dir)
    else:
	    project_dir = os.path.dirname(cess_file)

    sys.path.append(project_dir)

    images.PROJECT_DIR = project_dir
    typo.PROJECT_DIR = project_dir

    execfile(cess_file, globals())

    setup()

    if not(pycessing._setup_done):
        pycessing.size()

    drawing.setSurface(pycessing.screen)
    drawing.setBackground(127, 127, 127)
    images.SCREEN = pycessing.screen
    images.CTX = drawing.ctx
    typo.CTX = drawing.ctx
    typo.DRAWING = drawing

    while 1:
        pycessing.doEvents(pygame.fastevent.get())
        draw()
        pycessing.clock.tick(pycessing.framerate)
        drawing._blitToScreen()
        pygame.display.flip()
        if(pycessing.noLoop):
            while 1:
                events = pygame.event.get()
                for event in events:
                    if event.type == pygame.QUIT:
                        sys.exit(0)
                pycessing.clock.tick(30)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2].strip())
