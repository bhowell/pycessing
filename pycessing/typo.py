"""
    typo.py
    Copyright 2012 Brendan Howell (brendan@howell-ersatz.com)

    This file is part of PyCessing.

    PyCessing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCessing is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCessing.  If not, see <http://www.gnu.org/licenses/>.
"""

import pango
import math

PROJECT_DIR = None
DRAWING = None


class ScreenText:
    def __init__(self, text=None, fontName="Helvetica", size=30, color=(255, 255, 255)):
        self.text = text
        self.fontName = fontName
        self.surface = None
        self.setFont(fontName, size)
        self.setColor(color)
        self.rotation = 0.0

    def setText(self, text):
        self.text = text

    def listFonts(self):
        print "Fonts available:"
        fonts = []
        for font in DRAWING.families:
            fonts.append(font.get_name())
        fonts.sort()
        for font in fonts:
            print font

    def setFont(self, fontName, size=30):
        #TODO: throw an error message if font does not exist
        self.fontName = fontName
        self.size = size

    #TODO: reimpliment this so it actually works with pango
    def setFontFromFile(self, filename, size=30):
        #fp = open(filename)
        print "NOT IMPLEMENTED: sorry this does not work yet"
        return

    def setSize(self, size):
        self.size = size

    def setColor(self, *args):
        vals = args

        #deal with a tuple as argument
        if isinstance(args[0], tuple):
            vals = []
            for val in args[0]:
                vals.append(val)

        #now set the color as greyscale, rgb, or rgba
        if len(vals) == 1:
            self.color = (vals[0] / 255.0, vals[0] / 255.0, vals[0] / 255.0, 1.0)
        elif len(vals) == 3:
            self.color = (vals[0] / 255.0, vals[1] / 255.0, vals[2] / 255.0, 1.0)
        elif len(vals) == 4:
            self.color = (vals[0] / 255.0, vals[1] / 255.0, vals[2] / 255.0, vals[3] / 255.0)
        else:
            print "ScreenText: error invalid color specified."

    def rotate(self, angle):
        self.rotation = -math.pi * angle / 180.0

    def draw(self, x, y, width=-1):
        DRAWING.ctx.save()
        DRAWING.ctx.translate(x, y)
        DRAWING.ctx.rotate(self.rotation)
        r, g, b, a = self.color
        DRAWING.ctx.set_source_rgba(r, g, b, a)
        layout = DRAWING.pangoctx.create_layout()
        layout.set_font_description(pango.FontDescription(self.fontName + " " + str(self.size)))
        layout.set_width(width * pango.SCALE)
        layout.set_text(self.text)
        DRAWING.pangoctx.update_layout(layout)
        DRAWING.pangoctx.show_layout(layout)
        DRAWING.ctx.restore()

    def setItalic(self, val=True):
        self.font.set_italic(val)

    def setUnderline(self, val=True):
        self.font.set_underline(val)

    def setBold(self, val=True):
        self.font.set_bold(val)
