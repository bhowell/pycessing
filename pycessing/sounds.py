"""
    sounds.py
    Copyright 2009 Brendan Howell (brendan@howell-ersatz.com)

    This file is part of PyCessing.

    PyCessing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCessing is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCessing.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygame

class Sound:
    def __init__(self, fileName):
	self.fileName = fileName
	self.loaded = False

    def _loadSound(self):
        self.sound = pygame.mixer.Sound(self.fileName)
        self.loaded = True

    def play(self, loops=0, maxtime=0, fade_ms=0):
        if(not(self.loaded)):
            self._loadSound()
        return self.sound.play(loops, maxtime, fade_ms)

    def setVolume(self, volume):
        self.sound.set_volume(volume)

    def stop(self):
        self.sound.stop()

    def fadeout(self, time):
        self.sound.fadeout(time)

    def getBuffer(self):
        return self.sound.get_buffer()

    def getLength(self):
        return self.sound.get_length()

    def getVolume(self):
        return self.sound.get_volume()

    def getNum_channels(self):
        return self.sound.get_num_channels()
