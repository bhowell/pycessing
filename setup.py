"""
This is a setup.py script for PyCessing

Usage:
    python setup.py py2app
"""
import sys
from glob import glob
from setuptools import setup, find_packages

APP = ['pycessing.py']
DATA_FILES = []
VERSION = "0.3"

MAC_OPTIONS = {'argv_emulation': True, 
		       'includes': ['sip','PyQt4','PyQt4.QtGui','PyQt4.QtCore','pygame',
		             'numpy','Image','cairo','json','xml.dom.minidom'],
		        'excludes': ['PyQt4.QtDesigner', 'PyQt4.QtNetwork', 'PyQt4.QtOpenGL',
		             'PyQt4.QtScript', 'PyQt4.QtSql', 'PyQt4.QtTest', 
					 'PyQt4.QtWebKit', 'PyQt4.QtXml', 'PyQt4.phonon']}

if sys.platform == "darwin":
    extra_options = dict(
        app=APP,
        data_files=DATA_FILES,
        options={'py2app': MAC_OPTIONS},
        setup_requires=['py2app'],
	)
elif sys.platform == 'win32':
     extra_options = dict(
         setup_requires=['py2exe'],
         app=[APP],
     )
else:
     extra_options = dict(
         scripts = ['scripts/pycessing', 'scripts/pycess'],
         packages = ["pycessing"],
         package_data = {'pycessing': ['data/help/*.html',
                                        'data/help/media/css/*',
                                        'data/examples/*',
                                        'data/icons/*',
                                        'data/fonts/*',
                                        ]},
         version = VERSION,
         author = "Brendan Howell",
         author_email = "brendan@pycessing.org",
         license = "GPLv3",
         url = "http://pycessing.org",
         classifiers = [
            "Development Status :: 4 - Beta",
	    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
	    'Operating System :: MacOS :: MacOS X',
            'Operating System :: Microsoft :: Windows',
            'Operating System :: POSIX',
            'Programming Language :: Python',
         ],
     )

setup(
    name="PyCessing",
    **extra_options
)
